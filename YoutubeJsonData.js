const data = {
    "kind": "youtube#searchListResponse",
    "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/xrPuTGR901f0cn1OJD-TvnBKoYU\"",
    "nextPageToken": "CAoQAA",
    "regionCode": "IN",
    "pageInfo": {
        "totalResults": 140268,
        "resultsPerPage": 10
    },
    "items": [
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/f9U-_jwKu8erSSGtx1vpyjQpYgc\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "dgS_I4KgW4I"
            },
            "snippet": {
                "publishedAt": "2020-03-15T05:31:22.000Z",
                "channelId": "UCng-_rO4tL-MoYaFuNm9sOQ",
                "title": "React Native",
                "description": "In this course, we will talk about what React Native, What is it and Why is it used? My Website - http://dilipchandima.github.io/ Do you want to build a React ...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/dgS_I4KgW4I/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/dgS_I4KgW4I/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/dgS_I4KgW4I/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "Eigen Academy",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/U3J8R23yHW87oXFjbPIEdu94rkY\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "oEI8wzGndh8"
            },
            "snippet": {
                "publishedAt": "2020-03-13T17:11:27.000Z",
                "channelId": "UCJxPzqK3s81fgQtq_99qPPA",
                "title": "ReactNative ✕ Expoで簡単なTodoアプリ　デモ",
                "description": "",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/oEI8wzGndh8/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/oEI8wzGndh8/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/oEI8wzGndh8/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "takaya sugiyama",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/xlCqlUpP4bnLF-tgZVUFWPFnqRc\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "Chg8p_5fuqA"
            },
            "snippet": {
                "publishedAt": "2020-03-10T14:22:27.000Z",
                "channelId": "UCCwSB8rxJLKPsuLcm7RHoqg",
                "title": "【ReactNative】作成したアプリをGoogle Playストアに配信",
                "description": "ReactNativeでスマホアプリを開発をライブコーディングでお届けします。 エビングハウスの忘却曲線管理アプリを作成しています。 今回は、作成し...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/Chg8p_5fuqA/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/Chg8p_5fuqA/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/Chg8p_5fuqA/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "星直史 / スナップマート株式会社取締役CTO",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/unmBrXuofaNvdSIJSarFks77RLE\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "DqfTsyJSa9A"
            },
            "snippet": {
                "publishedAt": "2020-03-10T01:49:02.000Z",
                "channelId": "UCkgnZDuS9fM951TpyBTkNPg",
                "title": "ReactNative 2020 Ep9 UI TouchableOpacity",
                "description": "หากท่านอยากสนับสนุนเรา CMDev Channel ท่านสามารถช่วยเราได้ เพียงแค่แวะเข้าไปช...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/DqfTsyJSa9A/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/DqfTsyJSa9A/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/DqfTsyJSa9A/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "CMDev",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/Bb4-EmMs6cFdl6EdfhFv4Fm7MyQ\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "-FTvNjWLR6s"
            },
            "snippet": {
                "publishedAt": "2020-03-10T01:43:55.000Z",
                "channelId": "UCkgnZDuS9fM951TpyBTkNPg",
                "title": "ReactNative 2020 Ep8 UI Button",
                "description": "หากท่านอยากสนับสนุนเรา CMDev Channel ท่านสามารถช่วยเราได้ เพียงแค่แวะเข้าไปช...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/-FTvNjWLR6s/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/-FTvNjWLR6s/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/-FTvNjWLR6s/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "CMDev",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/mU4w2gsp8LkHiCTwQ0UAYa8Zybc\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "aO9l-cbS-bU"
            },
            "snippet": {
                "publishedAt": "2020-03-09T14:06:33.000Z",
                "channelId": "UCkgnZDuS9fM951TpyBTkNPg",
                "title": "ReactNative 2020 Ep3 UI Flex",
                "description": "หากท่านอยากสนับสนุนเรา CMDev Channel ท่านสามารถช่วยเราได้ เพียงแค่แวะเข้าไปช...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/aO9l-cbS-bU/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/aO9l-cbS-bU/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/aO9l-cbS-bU/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "CMDev",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/dZSTElu6-AMCLoHLEjVf5AVBUY0\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "NxJCSI7a8wk"
            },
            "snippet": {
                "publishedAt": "2020-02-03T15:01:04.000Z",
                "channelId": "UC4xKdmAXFh4ACyhpiQ_3qBw",
                "title": "Why React Native is garbage.",
                "description": "Ex-Google/ex-Facebook TechLead presents the case against the React Native cross-platform mobile app framework. Join my interview training prep here ...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/NxJCSI7a8wk/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/NxJCSI7a8wk/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/NxJCSI7a8wk/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "TechLead",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/zV_rJBgoibZZrcuTCAA04KCYkro\"",
            "id": {
                "kind": "youtube#playlist",
                "playlistId": "PL4cUxeGkcC9ixPU-QkScoRBVxtPPzVjrQ"
            },
            "snippet": {
                "publishedAt": "2019-11-25T08:15:02.000Z",
                "channelId": "UCW5YeuERMmlnqo4oq8vwUpg",
                "title": "React Native Tutorial for Beginners",
                "description": "",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/ur6I5m2nTvk/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/ur6I5m2nTvk/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/ur6I5m2nTvk/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "The Net Ninja",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/lBXoan4U2iKfIuOBxVGKsTOki-0\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "eR1vP-W1emI"
            },
            "snippet": {
                "publishedAt": "2019-10-23T13:00:07.000Z",
                "channelId": "UCPZlE8KsMkumnjEMOcJDxuQ",
                "title": "Build a React Native Chat App with Firebase in 20 MINUTES!",
                "description": "How simple is it build a Firebase Realtime Database & React Native chat app? So simple we can do it in less than 20 minutes! With the help of React Native ...",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/eR1vP-W1emI/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/eR1vP-W1emI/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/eR1vP-W1emI/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "DesignIntoCode",
                "liveBroadcastContent": "none"
            }
        },
        {
            "kind": "youtube#searchResult",
            "etag": "\"SJZWTG6xR0eGuCOh2bX6w3s4F94/abwmtNfYqtL7EGM3xEpvgP7RXNE\"",
            "id": {
                "kind": "youtube#video",
                "videoId": "eSjFDWYkdxM"
            },
            "snippet": {
                "publishedAt": "2019-10-18T12:04:36.000Z",
                "channelId": "UCSfwM5u0Kce6Cce8_S72olg",
                "title": "Emulando React Native no iOS/Android com Expo | Code/Drops #03",
                "description": "De um tempo pra cá o Expo evoluiu muito e desde então recomendo a todo iniciante começar no desenvolvimento React Native utilizando essa ferramenta.",
                "thumbnails": {
                    "default": {
                        "url": "https://i.ytimg.com/vi/eSjFDWYkdxM/default.jpg",
                        "width": 120,
                        "height": 90
                    },
                    "medium": {
                        "url": "https://i.ytimg.com/vi/eSjFDWYkdxM/mqdefault.jpg",
                        "width": 320,
                        "height": 180
                    },
                    "high": {
                        "url": "https://i.ytimg.com/vi/eSjFDWYkdxM/hqdefault.jpg",
                        "width": 480,
                        "height": 360
                    }
                },
                "channelTitle": "Rocketseat",
                "liveBroadcastContent": "none"
            }
        }
    ]
}

export default data;